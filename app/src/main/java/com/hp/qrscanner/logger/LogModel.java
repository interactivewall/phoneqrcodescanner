package com.hp.qrscanner.logger;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@SuppressWarnings("unused")
public class LogModel extends RealmObject {

    public static final int INFORMATION = 0;
    public static final int VERBOSE = 1;
    public static final int DEBUG = 2;
    public static final int WARNING = 3;
    public static final int ERROR = 4;

    @PrimaryKey
    private String id;

    private String className;

    private String methodName;

    private int level;

    private String message;

    private String stackTrace;

    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public static String getLevelName(int level) {
        String result;
        switch (level) {
            case LogModel.INFORMATION:
                result = "INFO";
                break;
            case LogModel.WARNING:
                result = "WARNING";
                break;
            case LogModel.ERROR:
                result = "ERROR";
                break;
            case LogModel.DEBUG:
                result = "DEBUG";
                break;
            case LogModel.VERBOSE:
                result = "VERBOSE";
                break;
            default:
                result = "";
        }
        return result;
    }
}
