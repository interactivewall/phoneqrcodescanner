package com.hp.qrscanner;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class Prefs {

    private static final String PREFS_FILE_NAME = "badge_scanner.prefs";
    private static final String PREF_BINARY_DATA_URL = "binaryDataUrl";
    private static final String PREF_GAME_LOGIC_URL = "gameLogicUrl";
    private static final String PREF_PLATFORM_ID = "platformId";

    public static void savePlatformId(Context context, String platformId) {
        getEditor(context).putString(PREF_PLATFORM_ID, platformId).apply();
    }

    public static String getPlatformId(Context context) {
        return getPreferences(context).getString(PREF_PLATFORM_ID, "platform1");
    }

    public static void saveBinaryDataUrl(Context context, String url) {
        getEditor(context).putString(PREF_BINARY_DATA_URL, url).apply();
    }

    public static String getBinaryDataUrl(Context context) {
        String result = getPreferences(context).getString(PREF_BINARY_DATA_URL, BuildConfig.BINARY_DATA_URL);
        if(TextUtils.isEmpty(result)) {
            result = BuildConfig.BINARY_DATA_URL;
        }
        if(result.charAt(result.length() - 1) != '/') {
            result += "/";
        }
        return result;
    }

    public static void saveGameLogicUrl(Context context, String url) {
        getEditor(context).putString(PREF_GAME_LOGIC_URL, url).apply();
    }

    public static String getGameLogicUrl(Context context) {
        String result = getPreferences(context).getString(PREF_GAME_LOGIC_URL, BuildConfig.GAME_LOGIC_URL);
        if(TextUtils.isEmpty(result)) {
            result = BuildConfig.BINARY_DATA_URL;
        }
        if(result.charAt(result.length() - 1) != '/') {
            result += "/";
        }
        return result;
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREFS_FILE_NAME, 0);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    private Prefs() {}

}
