package com.hp.qrscanner.network;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;

public interface BinaryDataApiService {

    // http://192.168.100.196:8020/quiz
    @PUT("/quiz")
    public Call<Object> quiz(@Body QuizRequest quizRequest);

}
