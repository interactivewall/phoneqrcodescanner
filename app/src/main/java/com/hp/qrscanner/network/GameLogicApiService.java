package com.hp.qrscanner.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GameLogicApiService {

    // http://192.168.100.196:5000/scan?platformId=baseStation1&cardId=[qrCode]
    @GET("/scan")
    Call<ScanResponse> scan(@Query("platformId") String platformId, @Query("cardId") String cardId);
}
