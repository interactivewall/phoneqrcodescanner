package com.hp.qrscanner.activities;

import android.content.DialogInterface;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.GsonBuilder;
import com.hp.qrscanner.BuildConfig;
import com.hp.qrscanner.Prefs;
import com.hp.qrscanner.R;
import com.hp.qrscanner.fragments.CameraSelectorDialogFragment;
import com.hp.qrscanner.logger.Log;
import com.hp.qrscanner.logger.LoggerActivity;
import com.hp.qrscanner.network.BinaryDataApiService;
import com.hp.qrscanner.network.GameLogicApiService;
import com.hp.qrscanner.network.QuizRequest;
import com.hp.qrscanner.network.ScanResponse;

import org.json.JSONObject;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler, CameraSelectorDialogFragment.CameraSelectorDialogListener {


    private static final String CAMERA_ID = "CAMERA_ID";
    private static final int LABEL_DELAY_MILLIS = 3000;

    private int cameraId;
    private ZBarScannerView scannerView;
    private TextView rightLabel;
    private TextView wrongLabel;
    private final Handler labelHandler = new Handler();
    private HideLabelRunnable currentHideLabelRunnable;

    private OkHttpClient okHttpClient;
    private BinaryDataApiService binaryDataApiService;
    private GameLogicApiService gameLogicApiService;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            cameraId = savedInstanceState.getInt(CAMERA_ID, 0);
        } else {
            cameraId = 0;
            int numberOfCameras = Camera.getNumberOfCameras();
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            for (int i = 0; i < numberOfCameras; i++) {
                Camera.getCameraInfo(i, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    cameraId = i;
                    break;
                }
            }
        }

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES);
        if(BuildConfig.DEBUG) {
            try {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Log.v(message);
                    }
                });
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addNetworkInterceptor(interceptor);
            } catch (Throwable t) {
                Log.e("Failed to add HttpLoggingInterceptor", t);
            }
        }
        okHttpClient = builder.build();

        setContentView(R.layout.activity_main);

        scannerView = (ZBarScannerView) findViewById(R.id.scanner);
        scannerView.setFormats(Collections.singletonList(BarcodeFormat.QRCODE));
        scannerView.setResultHandler(this);
        rightLabel = (TextView) findViewById(R.id.right_label);
        wrongLabel = (TextView) findViewById(R.id.wrong_label);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.startCamera(cameraId);
        scannerView.setFlash(false);
        scannerView.setAutoFocus(true);

        Retrofit gameLogicRetrofit = new Retrofit.Builder()
                .baseUrl(Prefs.getGameLogicUrl(this))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
        gameLogicApiService = gameLogicRetrofit.create(GameLogicApiService.class);

        Retrofit binaryDataRetrofit = new Retrofit.Builder()
                .baseUrl(Prefs.getBinaryDataUrl(this))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .build();
        binaryDataApiService = binaryDataRetrofit.create(BinaryDataApiService.class);

        rightLabel.setVisibility(View.GONE);
        wrongLabel.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(currentHideLabelRunnable != null) {
            labelHandler.removeCallbacks(currentHideLabelRunnable);
            currentHideLabelRunnable = null;
        }
        scannerView.stopCamera();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_scanner, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                SettingsActivity.start(this);
                return true;
            case R.id.action_logs:
                LoggerActivity.start(this);
                return true;
            case R.id.camera:
                DialogFragment cFragment = CameraSelectorDialogFragment.newInstance(cameraId);
                cFragment.show(getSupportFragmentManager(), "camera_selector");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleResult(Result result) {
        String cardId = result.getContents();
        if(!TextUtils.isEmpty(cardId)) {
            gameLogicApiService.scan(Prefs.getPlatformId(this), cardId).enqueue(new Callback<ScanResponse>() {
                @Override
                public void onResponse(Call<ScanResponse> call, Response<ScanResponse> response) {
                    Log.d("Game Logic request succeed");
                    if(response.isSuccessful()) {
                        if(response.body().answer) {
                            rightLabel.setVisibility(View.VISIBLE);
                            labelHandler.postDelayed(
                                    currentHideLabelRunnable = new HideLabelRunnable(rightLabel), LABEL_DELAY_MILLIS);
                        } else {
                            wrongLabel.setVisibility(View.VISIBLE);
                            labelHandler.postDelayed(
                                    currentHideLabelRunnable = new HideLabelRunnable(wrongLabel), LABEL_DELAY_MILLIS);
                        }
                    } else {
                        String errorMessage = parseServerError(response);
                        showAlert(errorMessage);
                        Log.e("Game Logic request failed with error message: " + errorMessage);
                    }
                }

                @Override
                public void onFailure(Call<ScanResponse> call, Throwable t) {
                    Log.e("Failed to execute Game Logic request", t);
                    showAlert("Game Logic request failed. " + t.getMessage());
                }
            });
            QuizRequest quizRequest = new QuizRequest();
            quizRequest.platformId = Prefs.getPlatformId(this);
            quizRequest.qrCode = cardId;
            binaryDataApiService.quiz(quizRequest).enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    if(response.isSuccessful()) {
                        Log.d("Binary Data request succeed");
                    } else {
                        String errorMessage = parseServerError(response);
                        scannerView.resumeCameraPreview(MainActivity.this);
                        Log.e("Binary Data request failed with error message: " + errorMessage);
                        scannerView.resumeCameraPreview(MainActivity.this);
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.e("Failed to execute Binary Data request", t);
                    showAlert("Binary Data request failed. " + t.getMessage());
                }
            });
        } else {
            scannerView.resumeCameraPreview(MainActivity.this);
        }
    }

    private String parseServerError(Response<?> response) {
        String errorMessage;
        int responseCode = response.code();
        if(responseCode == 500) {
            try {
                JSONObject jsonBody = new JSONObject(new String(response.errorBody().bytes()));
                errorMessage = jsonBody.getString("errorMsg");
            } catch (Exception e) {
                Log.e("Failed to parse error response from server", e);
                errorMessage = "Failed to send data. Error code " + responseCode + ".";
            }
        } else {
            errorMessage = "Failed to send data. " + responseCode + " " + response.message();
        }
        Log.e(errorMessage);

        return errorMessage;
    }

    private void showAlert(String errorMessage) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(errorMessage)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        scannerView.resumeCameraPreview(MainActivity.this);
                    }
                })
                .show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CAMERA_ID, cameraId);
    }

    @Override
    public void onCameraSelected(int newCameraId) {
        if (cameraId != newCameraId) {
            scannerView.stopCamera();
            cameraId = newCameraId;
            scannerView.startCamera(cameraId);
        }
    }

    private class HideLabelRunnable implements Runnable {

        private final View label;

        public HideLabelRunnable(View label) {
            this.label = label;
        }

        @Override
        public void run() {
            currentHideLabelRunnable = null;
            label.setVisibility(View.GONE);
            scannerView.resumeCameraPreview(MainActivity.this);
        }
    }
}
