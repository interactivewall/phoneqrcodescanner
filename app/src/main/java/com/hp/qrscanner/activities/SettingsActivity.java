package com.hp.qrscanner.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.hp.qrscanner.Prefs;
import com.hp.qrscanner.R;
import com.hp.qrscanner.logger.Log;

public class SettingsActivity extends AppCompatActivity {

    private EditText gameLogicServerUrlEditText;
    private EditText binaryDataServerUrlEditText;
    private EditText platformIdEditText;

    public static void start(Context context) {
        Intent i = new Intent(context, SettingsActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        gameLogicServerUrlEditText = (EditText) findViewById(R.id.game_logic_server_url);
        binaryDataServerUrlEditText = (EditText) findViewById(R.id.binary_data_server_url);
        platformIdEditText = (EditText) findViewById(R.id.platform_id);

        gameLogicServerUrlEditText.setText(Prefs.getGameLogicUrl(this));
        binaryDataServerUrlEditText.setText(Prefs.getBinaryDataUrl(this));
        platformIdEditText.setText(Prefs.getPlatformId(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                String gameLogicServerUrl = gameLogicServerUrlEditText.getText().toString();
                String oldGameLogicServerUrl = Prefs.getGameLogicUrl(this);
                if(!oldGameLogicServerUrl.equals(gameLogicServerUrl)) {
                    Log.i("Game Logic server url changed to " + gameLogicServerUrl);
                }
                Prefs.saveGameLogicUrl(this, gameLogicServerUrl);

                String binaryDataServerUrl = binaryDataServerUrlEditText.getText().toString();
                String oldBinaryDataServerUrl = Prefs.getBinaryDataUrl(this);
                if(!oldBinaryDataServerUrl.equals(binaryDataServerUrl)) {
                    Log.i("Binary Data server url changed to " + binaryDataServerUrl);
                }
                Prefs.saveBinaryDataUrl(this, binaryDataServerUrl);

                String platformId = platformIdEditText.getText().toString();
                String oldPlatformId = Prefs.getPlatformId(this);
                if(!oldPlatformId.equals(platformId)) {
                    Log.i("Platform Id changed to " + platformId);
                }
                Prefs.savePlatformId(this, platformId);
                finish();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
